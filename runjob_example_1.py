# coding: utf-8
#2016-4-17
#some definitions:
totalthread=3
baseurl="http://example.com?id=%s"
basedir='exp'

#imports
import sys,re,os,socket
import Jobcontrol_T as Jobcontrol
import logging

if sys.getdefaultencoding() != 'utf8':
	reload(sys)
	sys.setdefaultencoding( "utf-8" )

socket.setdefaulttimeout(60)
logger = logging.getLogger('Job.Main')
logger.setLevel(logging.INFO)

#functions
class TheJob(Jobcontrol.Jobclass):
	def __init__(self,param,useproxy=0,proxyaddr='http://127.0.0.1:8580'):
		super(TheJob, self).__init__(param,useproxy,proxyaddr)
		self.name='wrxqh'
		#define self.opener1
		#if opener is not cecessary uncommend the following line and commend the Jobclass.__init__()
		#self.param=param
	
	def do_job(self):
		fil='%s/%s/%s.txt' % (basedir,self.param,self.param)
		if (not os.path.isfile(fil)) or (os.path.getsize(fil)==0):
			fddata=Jobcontrol.fetch_url(baseurl % self.param,opener1=self.opener1)
			if not fddata:
				return 0
			rx=re.search('(.*)',fddata,re.I|re.S)
			if not rx:
				logger.error('Fail rx',self.param)
				return 0
			info=rx.group(1)#.replace(' ','')
			#info=re.sub(u'<.*?>|\t','',info)
			
			try:
				os.mkdir('%s/%s' % (basedir,self.param))
			except:
				pass
			f=open(fil,'w')
			f.write(info)
			f.close()
		else:
			f=open(fil,'r')
			info=f.read()
			f.close()
			
		foundlist=re.findall('<img.*?src="(.*?)" class=',info)+re.findall('<a href="(.*?)">',info)
		print foundlist
		
		for i in foundlist:
			if i[0]=='/':
				i='http://example.com'+i
			if i[-3:]=='Id=' or i[-3:]=='oad':
				continue
			
			fil='%s/%s/%s' % (basedir,self.param,i.split('/')[-1])
			if (not os.path.isfile(fil)) or (os.path.getsize(fil)==0):
				d=Jobcontrol.fetch_url(i,raw=1,opener1=self.opener1)
				with open(fil, "wb") as code:
					code.write(d)
					code.close()
		
		logger.critical('The job done.%s' % self.param)
		return 1


def fill_the_work(work):
	parameter_List=range(3000,3001)
	for the_arg in parameter_List:
		work.append((TheJob,the_arg))

"""
def Result_handler(result):
	job,rel=result
	#print job.name,rel
	#possible db work
"""

Jobcontrol.fill_the_work=fill_the_work
Jobcontrol.Result_handler=None

if __name__ == '__main__':
	Jobcontrol.multithread_run(totalthread)
	print "Job Done, main exit."
	logger.critical('Job Done, main exit.')