# coding: utf-8
#2016-6-19
#some definitions:
totalthread=3

#imports
import sys,socket
import Jobcontrol_T as Jobcontrol
import logging

if sys.getdefaultencoding() != 'utf8':
	reload(sys)
	sys.setdefaultencoding( "utf-8" )

socket.setdefaulttimeout(60)
logger = logging.getLogger('Job.Main')
logger.setLevel(logging.INFO)

#functions
class TheJob(Jobcontrol.Jobclass):
	def __init__(self,param,useproxy=0,proxyaddr='http://127.0.0.1:8580'):
		super(TheJob, self).__init__(param,useproxy,proxyaddr)
		self.name='TheJob'
		#define self.opener1
		#if opener is not cecessary uncommend the following line and commend the Jobclass.__init__()
		#self.param=param
	
	def do_job(self):
		print 'do job' 
		logger.critical('The job done.%s' % self.param)
		if 1:
			return 1
		else:
			return 0

def fill_the_work(work):
	parameter_List=xrange(10)
	for the_arg in parameter_List:
		work.append((TheJob,the_arg))

def Result_handler(result):
	job,rel=result
	#print job.name,rel
	#possible db work

Jobcontrol.fill_the_work=fill_the_work
Jobcontrol.Result_handler=Result_handler

if __name__ == '__main__':
	Jobcontrol.multithread_run(totalthread)
	logger.critical('Job Done, main exit.')