# coding: utf-8
#2017-12-15
#Use trip
#some definitions:
running=True

#imports
import sys,random
import trip
import logging
import logging.handlers


#some global inits:

#run.log
handler = logging.handlers.RotatingFileHandler('run.log', maxBytes = 1024*1024, backupCount = 5)    # 实例化handler
fmt = '%(asctime)s %(levelname)s %(name)s %(message)s'
handler.setFormatter(logging.Formatter(fmt))    # 为handler添加formatter
logger = logging.getLogger('Job')    # 获取名为tst的logger
logger.addHandler(handler)    # 为logger添加handler

#std.out
handler = logging.StreamHandler()
fmt = '%(asctime)s %(levelname)s %(name)s %(message)s'
handler.setFormatter(logging.Formatter(fmt))    # 为handler添加formatter
logger.addHandler(handler)    # 为logger添加handler

logger.setLevel(logging.DEBUG)

logger = logging.getLogger('Job.Lib')

#logger.info('first info message')  
#logger.debug('first debug message')  
#('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL')

#functions
#----------------------------Tick----------------------------
class tick():
    """
    def done(min,sec):
        print min,sec
    
    tk=tick(done,10000)
    while 1:
        tk.tick()
    """
    def __init__(self,done,loop=1000):
        self.loop=loop
        self.sec=self.loop
        self.done=done
        self.min=0

    def tick(self):
        self.sec=self.sec-1
        if self.sec<1:
            self.min=self.min+1
            self.sec=self.loop
            self.done(self.min,self.loop)

#----------------------------Net----------------------------
def randomip():#ip random
    return str(random.randint(0,254))+'.'+str(random.randint(0,254))+'.'+str(random.randint(0,254))+'.'+str(random.randint(0,254))

@trip.coroutine
def fetch_url(url,postdata=None,Session=None,cookie='',ref='',raw=0,retry=5):#fetch from url and then decode
    for _ in xrange(retry):
        try:
            fddata=yield fetch_url_1(url,postdata,Session,cookie,ref,raw)
            if fddata:
                break
            else:
                logger.warning('Network error! Empty return. %s' % url)
        except Exception,e:
            logger.warning('Network error! %s %s' % (e,url))
            if ("timed out" not in e) and ("IncompleteRead" not in e) and ("Connection reset by peer" not in e):
                raise trip.Return('')
    else:
        logger.error('Fetch fail! %s' % url)
        raise trip.Return('')
    logger.info('Fetch done %s' % url)
    raise trip.Return(fddata)

@trip.coroutine
def fetch_url_1(url,postdata=None,Session=None,cookie='',ref='',raw=0):#fetch from url and then decode
    if not Session:
        ip=randomip()
        headers={"User-Agent":"Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:27.0) Gecko/20100101 Firefox/27.0","X-FORWARDED-FOR":ip,"VIA":ip,"CLIENT-IP":ip,'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp",'Accept-Encoding':"*"}
        if cookie:
            headers["Cookie"]=cookie
        if ref:
            headers["Referer"]=ref
        Session=trip.Session()
        Session.headers.update(headers)
    if postdata:
        r=yield Session.post(url,data=postdata,timeout=5)
    else:
        r=yield Session.get(url,timeout=5)
    if raw:
        raise trip.Return(r.raw.read())
    else:
        raise trip.Return(r.content)

#----------------------------Exit----------------------------
class _Getch:
    """Gets a single character from standard input.  Does not echo to the screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()
    def __call__(self): return self.impl()

class _GetchUnix:
    def __init__(self):
        import tty, sys
    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno(), termios.TCSANOW)
            ch = sys.stdin.read(1)
            sys.stdout.write(ch)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

class _GetchWindows:
    def __init__(self):
        import msvcrt
    def __call__(self):
        import msvcrt
        return msvcrt.getch()

def findexit():
    global running
    while running:
        getch = _Getch()
        ch=getch()
        if ch=='c':
            logger.critical('Terminated by keyboard!')
            running=False
            sys.exit()

#----------------------------Controlling----------------------------
def multithread_run(total_thread=3,Result_handler=None):
    work=[]
    fill_the_work(work)
    def run_worker():
        yield [worker(work,Result_handler) for i in range(total_thread)]
    trip.run(run_worker)
    
    

@trip.coroutine
def worker(work,resulthandler=None):
    while work and running:
        #if not running:
        #    sys.exit()
        try:
            the_class,the_arg=work.pop()
        except:
            logger.critical('Pop error')
            raise trip.Return()
        try:
            job=the_class(the_arg)
        except Exception,e:
            logger.error("Job error. %s. Error: %s" % (the_arg,e))
        else:
            try:
                rel=yield job.do_job()
                if resulthandler:
                    resulthandler((job,rel))    #handle every result of a arg with job
                    #resulthandler('%s: %d<br>\n' % (job.name,rel))    #for web
            except Exception,e:
                logger.error("Job error. %s: %s. Error: %s" % (job.name,the_arg,e))
    logger.critical('Job done, thread exit.')

#----------------------------Task----------------------------
class Jobclass(object):
    def __init__(self,param,useproxy=0,proxyaddr='http://127.0.0.1:8580'):
        self.name='Jobclass_'
        ip=randomip()
        self.headers={"User-Agent":"Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:27.0) Gecko/20100101 Firefox/27.0","X-FORWARDED-FOR":ip,"VIA":ip,"CLIENT-IP":ip,'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp",'Accept-Encoding':"*"}
        if useproxy==1:
            prot=proxyaddr.split(":")
            self.Session=trip.Session(proxies={prot[0]:proxyaddr})
        else:
            self.Session=trip.Session()
        self.Session.headers.update(self.headers)
        self.param=param
        

    def do_job(self):
        print 'done'
        if 1:
            return 1
        else:
            return 0

def fill_the_work(work):
    parameter_List=[]
    for the_arg in parameter_List:
        work.append((Jobclass,the_arg))

def Result_handler(result):
    job,rel=result
    print job.name,rel


#----------------------------Main----------------------------
if __name__ == '__main__':
    multithread_run(3,Result_handler=Result_handler)
    logger.ciritcal('Job Done, main exit.')

