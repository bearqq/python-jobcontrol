# coding: utf-8
#2016-6-19
#some definitions:
totalthread=2

#imports
import sys,socket
import Jobcontrol_C as Jobcontrol
import logging

if sys.getdefaultencoding() != 'utf8':
	reload(sys)
	sys.setdefaultencoding( "utf-8" )

socket.setdefaulttimeout(60)
logger = logging.getLogger('Job.Main')
logger.setLevel(logging.INFO)

#functions
class TheJob(Jobcontrol.Jobclass):
	def __init__(self,param,useproxy=0,proxyaddr='http://127.0.0.1:8580'):
		super(TheJob, self).__init__(param,useproxy,proxyaddr)
		self.name='TheJob'
		#define self.opener1
		#if opener is not cecessary uncommend the following line and commend the Jobclass.__init__()
		#self.param=param
	
	@Jobcontrol.trip.coroutine
	def do_job(self):
		r=yield Jobcontrol.fetch_url('http://www.baidu.com/?x=%s' % self.param)
		logger.critical('The job done.%s' % self.param)
		#print r[:500]
		if 1:
			raise Jobcontrol.trip.Return(1)
		else:
			raise Jobcontrol.trip.Return(0)

def fill_the_work(work):
	parameter_List=xrange(3)
	for the_arg in parameter_List:
		work.append((TheJob,the_arg))

Jobcontrol.fill_the_work=fill_the_work
Jobcontrol.Result_handler=None

if __name__ == '__main__':
	Jobcontrol.multithread_run(totalthread)
	logger.critical('Job Done, main exit.')