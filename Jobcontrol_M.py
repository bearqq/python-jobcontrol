# coding: utf-8
#2016-6-19
#some definitions:
running=True

#imports
import sys,random
import threading
import urllib2,cookielib#, urllib
#import codecs,os
from cStringIO import StringIO
import gzip
from multiprocessing.dummy import Pool    #multi_thread
#from multiprocessing import Pool    #multi_process

import logging
import logging.handlers

if sys.getdefaultencoding() != 'utf8':
    reload(sys)
    sys.setdefaultencoding( "utf-8" )

#import webapp2sp as webapp2    #for web

#some global inits:

#run.log
handler = logging.handlers.RotatingFileHandler('run.log', maxBytes = 1024*1024, backupCount = 5)    # 实例化handler
fmt = '%(asctime)s %(levelname)s %(name)s %(message)s'
handler.setFormatter(logging.Formatter(fmt))    # 为handler添加formatter
logger = logging.getLogger('Job')    # 获取名为tst的logger
logger.addHandler(handler)    # 为logger添加handler

#std.out
handler = logging.StreamHandler()
fmt = '%(asctime)s %(levelname)s %(name)s %(message)s'
handler.setFormatter(logging.Formatter(fmt))    # 为handler添加formatter
logger.addHandler(handler)    # 为logger添加handler

logger.setLevel(logging.DEBUG)

logger = logging.getLogger('Job.Lib')

#logger.info('first info message')  
#logger.debug('first debug message')  
#('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL')

#functions
#----------------------------Tick----------------------------
class tick():
    """
    def done(min,sec):
        print min,sec
    
    tk=tick(done,10000)
    while 1:
        tk.tick()
    """
    def __init__(self,done,loop=1000):
        self.loop=loop
        self.sec=self.loop
        self.done=done
        self.min=0

    def tick(self):
        self.sec=self.sec-1
        if self.sec<1:
            self.min=self.min+1
            self.sec=self.loop
            self.done(self.min,self.loop)

#----------------------------Net----------------------------
def randomip():#ip random
    return str(random.randint(0,254))+'.'+str(random.randint(0,254))+'.'+str(random.randint(0,254))+'.'+str(random.randint(0,254))


def codeiss(data):#decode
    try:
        return data.decode("GB18030")
    except:
        return data.decode("UTF-8")
    """
    reg=re.compile('=\"utf-8|=utf-8|=\'utf-8',re.IGNORECASE).search(data)
    if reg and (reg.start()<400):
        return data.decode("UTF-8")
    else:
        return data.decode("GBK")
    """

def fetch_url(url,postdata=None,opener1=urllib2.build_opener(),cookie='',ref='',raw=0,retry=5):#fetch from url and then decode
    for _ in xrange(retry):
        try:
            fddata=fetch_url_1(url,postdata,opener1,cookie,ref,raw)
            if fddata:
                break
            else:
                logger.warning('Network error! Empty return. %s' % url)
        except Exception,e:
            logger.warning('Network error! %s %s' % (e,url))
            if ("timed out" not in e) and ("IncompleteRead" not in e) and ("Connection reset by peer" not in e):
                return ''
    else:
        logger.error('Fetch fail! %s' % url)
        return ''
    logger.info('Fetch done %s' % url)
    return fddata


def fetch_url_1(url,postdata=None,opener1=urllib2.build_opener(),cookie='',ref='',raw=0):#fetch from url and then decode
    request1 = urllib2.Request(url,data=postdata)
    request1.add_header("User-Agent","Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:27.0) Gecko/20100101 Firefox/27.0")
    if cookie:
        request1.add_header("Cookie",cookie)
    ip=randomip()
    request1.add_header("X-FORWARDED-FOR",ip)
    request1.add_header("VIA",ip)
    request1.add_header("CLIENT-IP",ip)
    request1.add_header('Accept', "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp")
    request1.add_header('Accept-Encoding', "*")
    if ref:
        request1.add_header("Referer",ref)

    fdbkf1=opener1.open(request1)

    if  fdbkf1.headers.get('Content-Encoding') == 'gzip':
        if raw:
            return gzip.GzipFile(fileobj=StringIO(fdbkf1.read())).read()
        else:
            return codeiss(gzip.GzipFile(fileobj=StringIO(fdbkf1.read())).read())
    else:
        if raw:
            return fdbkf1.read()
        else:
            return codeiss(fdbkf1.read())

#----------------------------Exit----------------------------
class _Getch:
    """Gets a single character from standard input.  Does not echo to the screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()
    def __call__(self): return self.impl()

class _GetchUnix:
    def __init__(self):
        import tty, sys
    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno(), termios.TCSANOW)
            ch = sys.stdin.read(1)
            sys.stdout.write(ch)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

class _GetchWindows:
    def __init__(self):
        import msvcrt
    def __call__(self):
        import msvcrt
        return msvcrt.getch()

def findexit():
    global running
    while running:
        getch = _Getch()
        ch=getch()
        if ch=='c':
            logger.critical('Terminated by keyboard!')
            running=False
            sys.exit()

#----------------------------Controlling----------------------------
def multithread_run(total_thread=3,Result_handler=None):
    work=[]
    fill_the_work(work)
    t=threading.Thread(target=findexit,args=())
    t.start()
    pool = Pool(total_thread)
    results=pool.map(worker,work)    #[(jobclass,1),(jobclass,3),(jobclass,2),(jobclass,4)] for 2 thread
    pool.close()
    pool.join()
    #t.join()
    
    if Result_handler:
        for result in results:
            Result_handler(result)

def worker(work):
    if running:
        the_class,the_arg=work
        try:
            job=the_class(the_arg)
        except Exception,e:
            logger.error("Job error. %s. Error: %s" % (the_arg,e))
        else:
            try:
                rel=job.do_job()
                #print 'Done ',job.name,the_arg
                return (job,rel)
            except Exception,e:
                logger.error("Job error. %s: %s. Error: %s" % (job.name,the_arg,e))
    else:
        sys.exit()

#----------------------------Task----------------------------
class Jobclass(object):
    def __init__(self,param,useproxy=0,proxyaddr='http://127.0.0.1:8580'):
        self.name='Jobclass_'
        self.cookie=cookielib.CookieJar()
        if useproxy==1:
            proxy_handler = urllib2.ProxyHandler({'http':proxyaddr})
            self.opener1=urllib2.build_opener(proxy_handler,urllib2.HTTPCookieProcessor(self.cookie))
        else:
            self.opener1=urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cookie))
        self.param=param

    def do_job(self):
        print 'done'
        if 1:
            return 1
        else:
            return 0

def fill_the_work(work):
    parameter_List=[]
    for the_arg in parameter_List:
        work.append((Jobclass,the_arg))

def Result_handler(result):
    job,rel=result
    print job.name,rel
    #self.response.write    #for web
    #possible db work

#----------------------------Main----------------------------
if __name__ == '__main__':
    multithread_run(3,Result_handler=Result_handler)
    logger.ciritcal('Job Done, main exit.')


"""    #for web
class MainPage(webapp2.RequestHandler):
    def get(self,total_thread=3):
        self.response.headers['Content-Type'] = 'text/html'
        work=[]
        fill_the_work(work)
        threads=[]
        for i in xrange(totalthread):
            t=threading.Thread(target=worker,args=(work,Result_handler,))
            threads.append(t)
        for i in threads:
            i.start()
        for i in threads:
            i.join()

application = webapp2.WSGIApplication([('/', MainPage),], debug=True)
webapp2.server(application)
"""
